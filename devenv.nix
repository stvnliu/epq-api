{ pkgs, lib, config, inputs, ... }:

{
  # https://devenv.sh/basics/
  # https://devenv.sh/packages/
  packages = [ pkgs.git ];

  # https://devenv.sh/scripts/

  enterShell = ''
    git --version
    echo "devenv up : to execute the epq chat application"
  '';

  # https://devenv.sh/tests/
  enterTest = ''
    echo "Running tests"
    git --version | grep "2.42.0"
  '';

  # https://devenv.sh/services/
  # services.postgres.enable = true;

  # https://devenv.sh/languages/
  languages = {
    java = {
      enable = true;
      jdk.package = pkgs.jdk17;
      maven = {
        enable = true;
      };
    };
  };


  # https://devenv.sh/pre-commit-hooks/
  pre-commit.hooks.shellcheck.enable = true;
  pre-commit.hooks.commitizen.enable = true;

  # https://devenv.sh/processes/
  processes = {
    epqchat-dev.exec = "${pkgs.maven}/bin/mvn spring-boot:run";
  };
  services.mysql = {
    enable = true;
    package = pkgs.mariadb;
    initialDatabases = [
      { name = "epqchat"; }
    ];
    ensureUsers = [
      {
        name = "epqchatuser";
        password = "epqlevel3artifact";
        ensurePermissions = {
          "epqchat.*" = "ALL PRIVILEGES";
          "*.*" = "SELECT, LOCK TABLES";
        };
      }
    ];
  };
  # See full reference at https://devenv.sh/reference/options/
}
